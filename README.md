# IP Endpoint Group Update

Linux script for scheduling an automatic update of a Skyetel Endpoint Group with your public-facing dynamic IP address.

This allows you to keep your PBX's public IP address updated.

# How to use

### Install git

CentOS 6/7
```
yum -y install git
```

Debian/Ubuntu
```
apt-get --assume-yes install git
```

### Clone this project to your PBX

```
git clone https://bitbucket.org/skyetel/ip-endpoint-group-update.git
```

### Logon to your account on https://login.skyetel.com 

Obtain an API Key (SID and Secret) by click on the gears in the upper-right corner. Record this securely so you can use it in the next step.

### Run the script


```
./ip-endpoint-group-update/ip-update-endpointgroup.sh
```

### Follow the directions to specify
- Port number (usually 5060)
- Transport (usually udp)
- Endpoint Group to Manage (or create a new one)


By default, your server will now check every 20 minutes to manage the IP address in your chosen Endpoint Group
