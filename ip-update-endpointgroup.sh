#!/bin/bash


# 'jq' is needed to parse JSON responses from the Skyetel API
# 'curl' is needed to interact with public URLs for retrieving and updating data
# 'cat' is needed to create cron script files
# 'sed' is needed to update settings in Asterisk
# 'logger' is needed by the scheduled task to direct messages properly
DEPENDENCIES=(jq curl cat sed logger)
SKYETEL_API_BASE_URL=${SKYETEL_API_BASE_URL:-https://api.skyetel.com/v1}
ASTERISK_EXTERNIP_CONF="/etc/asterisk/sip_general_custom.conf"

#
### DEFINE FUNCTIONS
#

# Borrowed from: https://stackoverflow.com/questions/296536/how-to-urlencode-data-for-curl-command
rawurlencode() {
    local string="${1}"
    local strlen=${#string}
    local encoded=""
    local pos c o

    for (( pos=0 ; pos<strlen ; pos++ )); do
        c=${string:$pos:1}
        case "$c" in
            [-_.~a-zA-Z0-9] ) o="${c}" ;;
            * )               printf -v o '%%%02x' "'$c"
        esac
        encoded+="${o}"
    done
    echo "${encoded}"
}

api_error_check() {
    if [ "$1" != "null" ] && [ "$1" != "" ]; then
        error_log "$API_ERROR"
        return 1
    else
        return 0
    fi
}

call_api() {
    local URL="$1"
    local API_QUERY=""

    # Expect query parameter if the first character is a ?
    if [ "${3:0:1}" == "?" ]; then
        local API_QUERY="$3"

    # Otherwise expect data
    elif [ "$3" != "" ]; then
        local OPT_DATA="--data $3"
    else
        local OPT_DATA=""
    fi

    API_RESULT=`curl -s -k -X $2 \
        -H "Origin: https://script.skyetel.com" \
        -H "X-AUTH-SID: ${SKYETEL_API_SID}" \
        -H "X-AUTH-SECRET: ${SKYETEL_API_SECRET}" \
        ${OPT_DATA} \
        "$SKYETEL_API_BASE_URL/$URL${API_QUERY}"`

    API_ERROR=`echo $API_RESULT | jq -r '.ERROR' 2> /dev/null`

    api_error_check "$API_ERROR"
    return $?
}

post_api() {
    call_api $1 POST "$2"
    return $?
}

patch_api() {
    call_api $1 PATCH "$2"
    return $?
}

get_api() {
    call_api $1 GET "?`rawurlencode "page[limit]"`=200"
    return $?
}

delete_api() {
    call_api $1 DELETE
    return $?
}

get_endpoint_group_id() {
    get_endpoint_groups || return $?

    API_RESULT=`echo $API_RESULT | jq -r --arg v "$1" '.[] | select( .id == ($v|tonumber) )' 2>/dev/null`

    if [ "$?" -ne 0 ] || [ "$API_RESULT" = "" ] || [ "$API_RESULT" = "null" ]; then
        # Nothing found
        return 1
    else
        return 0
    fi
}

get_endpoint_groups() {
    get_api "endpoints/groups"
    return $?
}

get_public_ip() {
    # Reach out to ipecho for feedback on the public IP address
    local PROVIDER_CMDS=( "curl -s https://ipecho.net/plain" "curl -s ifconfig.co" "curl -s ifconfig.me" )
    
    # Try providers until one gives us what we're looking for
    for PCMD in "${PROVIDER_CMDS[@]}"; do
        local RETRIEVE_IP=`$PCMD`

        # Simplified regex to roughly match an IPv4 (Yes I know it could be out of bounds, but this is basic sanity checking)
        local RE_IP='^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$'

        if [[ "${RETRIEVE_IP}" =~ $RE_IP ]]; then
            PUBLIC_IP="$RETRIEVE_IP"
            return 0
        fi
    done

    # If we made it here
    error_log "Could not retrieve a valid public-facing IPv4 address"
    return 1
}

create_endpoint_with_group() {
    #"ip=$2&port=$3&flags=$4&priority=$5&attrs=$6&description=$7&endpoint_group_name=$8"
    post_api "endpoints" "$1"
    return $?
}

get_endpoint_group_members() {
    get_api "endpoints/groups/$1/relationships"
    return $?
}

add_ip_to_endpoint_group_id() {
    post_api
    return $?
}

delete_endpoint_id() {
    delete_api "endpoints/$1"
    return $?
}

patch_endpoint_id() {
    patch_api "endpoints/$1" "$2"
    return $?
}

update_endpoint_group() {
    check_required_environment || return 1
    get_public_ip || return 1

    get_endpoint_group_members "$SKYETEL_EG_ID" || return 1

    FIND_IP=`echo $API_RESULT | jq -r --arg IP "$PUBLIC_IP" --arg PORT "$SKYETEL_EP_PORT" '.[] | select( .ip == $IP )' 2>/dev/null`
    NON_IP_MEMBERS=`echo $API_RESULT | jq -r --arg IP "$PUBLIC_IP" '[ .[] | select( .ip != $IP ) ]' 2>/dev/null`

    # Re-use the current member if there's only one that doesn't match on IP
    NUM_NON_MEMBERS=$(echo $NON_IP_MEMBERS | jq '. | length')
    if [ "$NUM_NON_MEMBERS" != "" ] && [ "$NUM_NON_MEMBERS" -eq 1 ] && [ "$FIND_IP" = "" ]; then
        FIND_IP=$(echo $NON_IP_MEMBERS | jq '.[0]')
        NON_IP_MEMBERS=""
    fi

    # Check for presence of IP
    if [ "$FIND_IP" = "" ] || [ "$FIND_IP" = "null" ]; then
        EP_DESC_ENCODED="`rawurlencode "Automatically Managed by Skyetel Script"`"
        API_ARGS="ip=$PUBLIC_IP&port=$SKYETEL_EP_PORT&transport=$SKYETEL_EP_TRANSPORT&priority=10&flags=0&attrs=&endpoint_group_id=$SKYETEL_EG_ID&description=$EP_DESC_ENCODED"
        echo "Adding: $API_ARGS"
        create_endpoint_with_group "$API_ARGS" || return 1
        apply_asterisk_externip || return 1
    else
        # Check if Endpoint needs PORT or TRANSPORT updated
        EP_ID="`echo $FIND_IP | jq -r '.id'`"
        CHECK_IP="`echo $FIND_IP | jq -r '.ip'`"
        CHECK_PORT="`echo $FIND_IP | jq -r '.port'`"
        CHECK_TRANSPORT="`echo $FIND_IP | jq -r '.transport'`"
        EP_DESC_ENCODED="`rawurlencode "Automatically Managed by Skyetel Script"`"

        if [ "$CHECK_IP" != "$PUBLIC_IP" ] || [ "$CHECK_PORT" != "$SKYETEL_EP_PORT" ] || [ "$CHECK_TRANSPORT" != "$SKYETEL_EP_TRANSPORT" ]; then
            echo "Updating Endpoint ID: $EP_ID (ip: $PUBLIC_IP, port: $SKYETEL_EP_PORT, transport: $SKYETEL_EP_TRANSPORT)"
            patch_endpoint_id "$EP_ID" "ip=$PUBLIC_IP&port=$SKYETEL_EP_PORT&transport=$SKYETEL_EP_TRANSPORT&description=$EP_DESC_ENCODED" || return 1
            apply_asterisk_externip || return 1
        fi
    fi

    # Remove IP Members that are unmanaged
    if [ ! "$NON_IP_MEMBERS" = "" ] && [ ! "$NON_IP_MEMBERS" = "null" ]; then
        ID_LIST=( `echo $NON_IP_MEMBERS | jq -r '.[] | .id'` )
        for i in ${ID_LIST[@]}; do
            echo "Deleting `echo $NON_IP_MEMBERS | jq -r --arg ID "$i" '.[] | select( .id == $ID ) | .ip'` from managed Endpoint Group"
            delete_endpoint_id "$i" || return 1
        done
    fi

    return 0
}

check_dependencies() {
    # Check for required dependencies

    local RETVAL=0
    for d in ${DEPENDENCIES[@]}; do
        hash $d 2> /dev/null
        if [ "$?" -ne 0 ]; then
            error_log "Required command '$d' does not exist and must be installed first."
            local RETVAL=1
        fi
    done

    return $RETVAL
}

install_dependencies() {
    # Try a yum or apt-get install
    hash yum 2> /dev/null
    if [ "$?" -eq 0 ]; then
        yum -y --enablerepo=epel install coreutils curl sed jq 2>&1 > /dev/null || return 1        
        return 0
    else
        hash apt-get 2> /dev/null
        if [ "$?" -eq 0 ]; then
            apt-get update 2>&1 > /dev/null || return 1
            apt-get --assume-yes install coreutils curl sed jq 2>&1 > /dev/null || return 1
            return 0
        fi
    fi

    # Nothing worked so we end up here failing
    return 1
}


check_required_environment() {
    local RETVAL=0

    if [ "${#SKYETEL_API_SID}" -lt 10 ]; then
        error_log "Invalid SKYETEL_API_SID value"
        local RETVAL=1
    fi

    if [ "${#SKYETEL_API_SECRET}" -lt 10 ]; then
        error_log "Invalid SKYETEL_API_SECRET value"
        local RETVAL=1
    fi

    local RE='^[0-9]+$'
    if ! [[ ${SKYETEL_EG_ID} =~ $RE ]] ; then
        error_log "Expected SKYETEL_EG_ID as numeric integer"
        local RETVAL=1
    fi

    SKYETEL_EP_PORT="${SKYETEL_EP_PORT:-5060}"
    if ! [[ ${SKYETEL_EP_PORT} =~ $RE ]]; then
        error_log "Expected SKYETEL_EP_PORT as numeric integer"
        local RETVAL=1
    fi

    SKYETEL_EP_TRANSPORT="${SKYETEL_EP_TRANSPORT:-udp}"
    local RE='^udp|tcp$'
    if ! [[ ${SKYETEL_EP_TRANSPORT} =~ $RE ]]; then
        error_log "Expected SKYETEL_EP_TRANSPORT as 'udp' or 'tcp'"
        local RETVAL=1
    fi

    return $RETVAL
}

apply_asterisk_externip() {

    # if file does not exist then create one
    if [ ! -f $ASTERISK_EXTERNIP_CONF ]; then
        echo "externip=$PUBLIC_IP" >> $ASTERISK_EXTERNIP_CONF || return 1
    else
        sed -i 's/^\s*externip\s*=.*$/externip='"$PUBLIC_IP"' ;Automatically managed by Skyetel Script/' $ASTERISK_EXTERNIP_CONF \
            || echo "externip=$PUBLIC_IP  ;Automatically managed by Skyetel Script" >> $ASTERISK_EXTERNIP_CONF || return 1
    fi

    # Attempt to reload
    echo "Reloading Asterisk"
    service asterisk reload 2>&1 > /dev/null
    
    if [ "$?" -ne 0 ]; then
        error_log "Failed to reload Asterisk new configuration"
        return 1
    fi

    return 0
}

handle_command_args() {
    local POSITIONAL=()
    while [[ $# -gt 0 ]]
    do
        local key="$1"

        case $key in
            -f|--force)
            FORCE_IGNORE="true"
            shift # past argument
            ;;
            -r|--reinstall)
            SKYETEL_REINSTALL="true"
            shift # past argument
            ;;
            -p|--port)
            SKYETEL_EP_PORT_ARG="$2"
            shift # past argument
            shift # past value
            ;;
            -t|--transport)
            SKYETEL_EP_TRANSPORT_ARG="$2"
            shift # past argument
            shift # past value
            ;;
            -k|--sid)
            SKYETEL_API_SID_ARG="$2"
            shift # past argument
            shift # past value
            ;;
            -s|--secret)
            SKYETEL_API_SECRET_ARG="$2"
            shift # past argument
            shift # past value
            ;;
            -e|--endpoint-group-id)
            SKYETEL_EG_ID_ARG="$2"
            shift # past argument
            shift # past value
            ;;
            *)    # unknown option
            POSITIONAL+=("$1") # save it in an array for later
            shift # past argument
            ;;
        esac
    done
    set -- "${POSITIONAL[@]}" # restore positional parameters

    return 0
}

error_log() {
    echo "$1" >&2
}


# Check if ready to run (if all environment variables are preset and not run in terminal)
if [ ! -z "$SKYETEL_API_SID" ] && [ ! -z "$SKYETEL_API_SECRET" ] && [ ! -z "$SKYETEL_EG_ID" ]; then
    check_dependencies || exit 1
    check_required_environment || exit 1

    get_public_ip || exit 1
    update_endpoint_group || exit 1
    exit 0
fi

##
### BEGIN INSTALLATION
##

if [[ $EUID -ne 0 ]]; then
    # Root is needed to perform installations. It may not be needed for updating IP addresses later, though.
    echo "This script must be run as root" 
    exit 1
fi

# Handle command-line arguments
handle_command_args "$@" || exit 1

# Check if already installed
SELF_SCRIPT_PATH="$0"
SELF_SCRIPT="${SELF_SCRIPT_PATH##*/}"
SELF_SCRIPT_NAME="${SELF_SCRIPT%.*}"
CRON_FILE="/etc/cron.d/${SELF_SCRIPT_NAME}.cron"
RUN_SCRIPT="/usr/local/bin/${SELF_SCRIPT}"
if [ -f $CRON_FILE ] && [ -f $RUN_SCRIPT ]; then
    # Check if the script is being run headless
    tty -s 2>&1 > /dev/null
    if [ "$FORCE_IGNORE" != "true" ] && [ "$?" -ne 0 ]; then
        error_log "${SELF_SCRIPT} is improperly configured; please reinstall"
        exit 1
    elif [ "$SKYETEL_REINSTALL" != "true" ]; then
        read -p "Script is already installed. Would you like to reinstall (Y/N)? " -n 1 -r
        echo

        if [[ ! $REPLY =~ ^[Yy]$ ]]; then
            # Exit here if not replied 'y' to reinstall
            echo "Exiting..."
            exit 0
        fi
    fi
fi

# Check if the script is being run headless
tty -s 2>&1 > /dev/null
if [ "$FORCE_IGNORE" != "true" ] && [ "$?" -ne 0 ]; then
    error_log "Script is not installed and cannot be run headless"
    exit 1 
fi

check_dependencies || install_dependencies || exit 1


# Check for required values
get_public_ip || exit 1
echo "Your public-facing IPv4 is: $PUBLIC_IP"
echo 

# Check for pre-existing settings
if [ -f $CRON_FILE ]; then
    source $CRON_FILE 2>&1 > /dev/null
fi

RE='^[0-9]+$'
while [ true ]; do
    if [[ $SKYETEL_EP_PORT_ARG =~ $RE ]]; then
        SKYETEL_EP_PORT="$SKYETEL_EP_PORT_ARG"
        break
    fi

    SKYETEL_EP_PORT_DEFAULT="${SKYETEL_EP_PORT:-5060}"
    read -e -p "Enter the SIP port [$SKYETEL_EP_PORT_DEFAULT]: " SKYETEL_EP_PORT
    SKYETEL_EP_PORT="${SKYETEL_EP_PORT:-$SKYETEL_EP_PORT_DEFAULT}"

    if [[ $SKYETEL_EP_PORT =~ $RE ]]; then
        break
    fi
done

RE='^udp|tcp$'
while [ true ]; do
    if [[ $SKYETEL_EP_TRANSPORT_ARG =~ $RE ]]; then
        SKYETEL_EP_TRANSPORT="$SKYETEL_EP_TRANSPORT_ARG"
        break
    fi

    SKYETEL_EP_TRANSPORT="${SKYETEL_EP_TRANSPORT:-udp}"

    if [ "$SKYETEL_EP_TRANSPORT" = "udp" ]; then
        SKYETEL_EP_TRANSPORT_VAL_DEFAULT="0"
    elif [ "$SKYETEL_EP_TRANSPORT" = "tcp" ]; then
        SKYETEL_EP_TRANSPORT_VAL_DEFAULT="1"
    fi

    read -p "Enter the transport 0 = UDP, 1 = TCP [$SKYETEL_EP_TRANSPORT_VAL_DEFAULT]: " -n 1 SKYETEL_EP_TRANSPORT_VAL

    SKYETEL_EP_TRANSPORT_VAL="${SKYETEL_EP_TRANSPORT_VAL:-$SKYETEL_EP_TRANSPORT_VAL_DEFAULT}"

    if [ "$SKYETEL_EP_TRANSPORT_VAL" -eq 0 ]; then
        SKYETEL_EP_TRANSPORT="udp"
    elif [ "$SKYETEL_EP_TRANSPORT_VAL" -eq 1 ]; then
        SKYETEL_EP_TRANSPORT="tcp"
    else
        SKYETEL_EP_TRANSPORT=""
    fi
        
    if [[ $SKYETEL_EP_TRANSPORT =~ $RE ]]; then
        break
    fi

done

echo
echo

echo "To continue, we need a Skyetel API SID and Secret."
echo "These can be created and retrieved from your account on https://login.skyetel.com"
echo

# Use default values if available
if [ ! "$SKYETEL_API_SID" = "" ]; then
    SKYETEL_API_SID_DEFAULT="$SKYETEL_API_SID"
    SKYETEL_API_SID_DEFAULT_TEXT=" [$SKYETEL_API_SID]"
else
    SKYETEL_API_SID_DEFAULT=""
    SKYETEL_API_SID_DEFAULT_TEXT=""
fi

if [ ! "$SKYETEL_API_SECRET" = "" ]; then
    SKYETEL_API_SECRET_DEFAULT="$SKYETEL_API_SECRET"
    SKYETEL_API_SECRET_DEFAULT_TEXT=" [$SKYETEL_API_SECRET]"
else
    SKYETEL_API_SECRET_DEFAULT=""
    SKYETEL_API_SECRET_DEFAULT_TEXT=""
fi

# Skip and use args if provided
if [ ! "$SKYETEL_API_SID_ARG" = "" ]; then
    SKYETEL_API_SID="$SKYETEL_API_SID_ARG"
else
    read -e -p "Enter your API SID${SKYETEL_API_SID_DEFAULT_TEXT}: " -r SKYETEL_API_SID
fi

if [ ! "$SKYETEL_API_SECRET_ARG" = "" ]; then
    SKYETEL_API_SECRET="$SKYETEL_API_SECRET_ARG"
else
    read -e -p "Enter your API Secret${SKYETEL_API_SECRET_DEFAULT_TEXT}: " -r SKYETEL_API_SECRET
fi

# Use defaults if available and still unset
SKYETEL_API_SID="${SKYETEL_API_SID:-$SKYETEL_API_SID_DEFAULT}"
SKYETEL_API_SECRET="${SKYETEL_API_SECRET:-$SKYETEL_API_SECRET_DEFAULT}"

echo
echo "We are now trying to use your credentials to gather more information..."

while [ true ]; do
    get_endpoint_groups || exit 1

    echo $API_RESULT | jq -r '.[] | [.id, .name] | @csv' 2>/dev/null
    echo

    # If already set
    if [ "$SKYETEL_EG_ID" != "" ]; then
        SKYETEL_EG_ID_DEFAULT="$SKYETEL_EG_ID"
        SKYETEL_EG_ID_DEFAULT_TEXT=" [$SKYETEL_EG_ID]"
    fi

    if [ "$SKYETEL_EG_ID_ARG" != "" ]; then
        SKYETEL_EG_ID="$SKYETEL_EG_ID_ARG"
    else
        echo "Enter 'x' to create a new Endpoint Group to manage"
        read -e -p "Enter the ID of the Endpoint Group you would like to be managed$SKYETEL_EG_ID_DEFAULT_TEXT: " -r SKYETEL_EG_ID
    fi

    SKYETEL_EG_ID="${SKYETEL_EG_ID:-$SKYETEL_EG_ID_DEFAULT}"

    RE='^[0-9]+$'
    if [[ $SKYETEL_EG_ID =~ $RE ]]; then
        get_endpoint_group_id $SKYETEL_EG_ID || continue
        check_required_environment || exit 1
        update_endpoint_group || exit 1
        break
    else
        #
        ## CREATE NEW ENDPOINTGROUP WITH ENDPOINT
        #
        echo
        while [ true ]; do
            read -e -p "Enter a new Endpoint Group Name: " -r EG_NAME
            if [ "$EG_NAME" = "" ] || [ "${#EG_NAME}" -gt 255 ]; then
                error_log "Invalid Endpoint Group Name. Try again"
                continue
            else
                # Protect the string by escaping characters
                break
            fi
        done

        # Escaping if needed, printf -v EG_NAME_ESCAPED "%q" "$EG_NAME"
        EG_NAME_ENCODED="`rawurlencode "$EG_NAME"`"
        EP_DESC_ENCODED="`rawurlencode "Automatically Managed by Skyetel Script"`"
        API_ARGS="ip=$PUBLIC_IP&port=$SKYETEL_EP_PORT&transport=$SKYETEL_EP_TRANSPORT&priority=10&flags=0&attrs=&endpoint_group_name=$EG_NAME_ENCODED&description=$EP_DESC_ENCODED"
        echo "$API_ARGS"
        create_endpoint_with_group "$API_ARGS" || exit 1
        SKYETEL_EG_ID=`echo $API_RESULT | jq -r '.endpoint_group.id'`

        RE='^[0-9]+$'
        if ! [[ ${SKYETEL_EG_ID} =~ $RE ]] ; then
            error_log "Creating an Endpoint Group failed"
            exit 1
        fi
        break
    fi
done


# Install self
cp -f $0 $RUN_SCRIPT

if [ "$?" -ne 0 ]; then
    error_log "Failed to install script"
    exit 1
fi

# Install cron job
cat << EOF > $CRON_FILE
SKYETEL_API_SID="$SKYETEL_API_SID"
SKYETEL_API_SECRET="$SKYETEL_API_SECRET"
SKYETEL_EG_ID="$SKYETEL_EG_ID"
SKYETEL_EP_PORT="$SKYETEL_EP_PORT"
SKYETEL_EP_TRANSPORT="$SKYETEL_EP_TRANSPORT"
SHELL=/bin/bash
PATH=$PATH

*/20    *   *   *   *   root $RUN_SCRIPT 2>&1 | logger -p local0.notice -t `basename $0`
EOF

if [ "$?" -ne 0 ]; then
    error_log "Failed to install scheduled task"
    exit 1
fi
